const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    output: {
        library: 'semanticMap',
        libraryTarget: 'umd',
    },
    externals: {
        leaflet: 'L',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ],
            },
            {
                test: /\.(png|jpg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]',
                    },
                },
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'showcase.html',
            template: './src/showcase.html',
            inject: false,
            minify: {
                removeComments: false,
                collapseWhitespace: false,
            },
        }),
        new CopyWebpackPlugin([
            {
                from: 'node_modules/leaflet/dist/',
                to: 'leaflet/',
            },
            {
                from: 'node_modules/leaflet.markercluster/dist/',
                to: 'leaflet.markercluster/',
            },
        ]),
    ],
};
