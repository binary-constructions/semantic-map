export as namespace semanticMap;

export function load(): Promise<any>;
export function mount(whenMounted: (maps: any) => void): void;
