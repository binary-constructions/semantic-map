import { Parser } from "../lib/Parser";
import { Renderer } from "../lib/Renderer";
import { Maps } from "../lib/Maps";

export async function load() {
    return new Maps(new Parser(document.body), Renderer);
}

export function mount(whenMounted) {
    load().then(
        value => {
            if (value) {
                value.mount();

                if (whenMounted) {
                    whenMounted(value);
                }
            }
        },
        reason => console.error('loading of semantic-map failed: ' + reason)
    );
}
