export async function load() {
    const renderer = await import(/* webpackChunkName: "Renderer" */ "./index");

    console.log("map renderer loaded");

    return renderer;
}
