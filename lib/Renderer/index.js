import L from "leaflet";
import "leaflet.markercluster";

export class Renderer {
    constructor() {
        this._description = undefined;
        this._leaflet_map = undefined;
        this._canvas_renderer = undefined;
        this._geojson_layer = undefined;
        this._geojson_layer_no_clustering = undefined;
        this._cluster_group = undefined;
        this._location_radius = undefined;
        this._location_center = undefined;
        this._icons = {};
    }

    _clear_features() {
        this._geojson_layer.remove();

        if (this._geojson_layer_no_clustering) {
            this._geojson_layer_no_clustering.remove();
        }

        if (this._cluster_group) {
            this._cluster_group.remove();
        }
    }

    _create_geojson_layer() {
        return L.geoJSON(undefined, {
            pointToLayer: (feature, latlng) => {
                if (feature.properties.circle) {
                    return L.circleMarker(latlng, {
                        renderer: this._canvas_renderer,
                        color: (feature.properties.circle instanceof Object && feature.properties.circle["color"]) || '#ffffff',
                        opacity: (feature.properties.circle instanceof Object && feature.properties.circle["opacity"]) || 0.8,
                        weight: feature.properties.circle instanceof Object && feature.properties.circle.hasOwnProperty("weight") ? feature.properties.circle["weight"] : 2,
                        fillOpacity: (feature.properties.circle instanceof Object && feature.properties.circle["fillOpacity"]) || 0.4,
                    });
                } else if (feature.properties.icon && this._icons.hasOwnProperty(feature.properties.icon)) {
                    return L.marker(latlng, { icon: this._icons[feature.properties.icon] });
                } else {
                    if (this._icons.hasOwnProperty('')) { // use the (nameless) default icon if there is any
                        return L.marker(latlng, { icon: this._icons[''] });
                    }

                    return L.marker(latlng);
                }
            },
            onEachFeature: (feature, layer) => {
                const popup = this.popupFromFeature(feature);

                if (popup) {
                    layer.bindPopup(popup);
                }

                const clickable = this.featureIsClickable(feature);

                const draggable = this.featureIsDraggable(feature);

                if (clickable || draggable) {
                    if (clickable) {
                        layer.on('click', event => {
                            if (this.onFeatureClick) {
                                this.onFeatureClick(event.target.feature);

                                L.DomEvent.stopPropagation(event);
                            }
                        });
                    }

                    if (draggable) {
                        layer.options.draggable = true;

                        layer.on('dragend', event => {
                            if (this.onFeatureDrag) {
                                const { lng, lat } = event.target.getLatLng();

                                this.onFeatureDrag(event.target.feature, lng, lat)
                            }
                        });
                    }
                } else {
                    layer.options.interactive = false;
                }
            },
        });
    }

    _load_features() {
        this._geojson_layer = this._create_geojson_layer();

        if (this._description.props.clustering_enabled) {
            this._geojson_layer_no_clustering = this._create_geojson_layer();
        }

        for (const feature of this._description.props.geojson_features) {
            const style = this.styleFromFeature(feature);

            const allow_clustering = this.clusterFilter(feature);

            const layer = !this._description.props.clustering_enabled || !allow_clustering
                  ? this._geojson_layer
                  : this._geojson_layer_no_clustering

            if (style) {
                layer.addData(feature).setStyle(style);
            } else {
                layer.addData(feature);
            }
        }

        if (this._description.props.clustering_enabled) {
            this._cluster_group = L.markerClusterGroup({ showCoverageOnHover: false });

            this._leaflet_map.addLayer(this._cluster_group);

            this._cluster_group.addLayer(this._geojson_layer);

            this._leaflet_map.addLayer(this._geojson_layer_no_clustering);
        } else {
            this._leaflet_map.addLayer(this._geojson_layer);
        }

        if (this._description.props.fit_features) {
            this._leaflet_map.fitBounds(this._geojson_layer.getBounds(), { padding: [40, 40] });
        }
    }

    mount(description) {
        this._description = description;

        if (this._leaflet_map !== undefined) {
            console.error("map was already mounted");

            return;
        }

        if (description.node.nodeName !== "DIV") {
            console.error("Leaflet will only mount on <div> elements");

            return;
        }

        if (this._description.props.icons) {
            for (const [name, data] of Object.entries(this._description.props.icons)) {
                this._icons[name || ''] = L.icon({
                    iconUrl: encodeURI("data:image/svg+xml," + data.svg).replace('#','%23'),
                    iconSize: [data.size || 20, data.size || 20],
                });
            }
        }

        this._leaflet_map = L.map(
            this._description.node,
            {
                minZoom: this._description.props.min_zoom,
                zoomControl: !this._description.props.hide_zoom_control,
            }
        ).setView(
            [
                this._description.props.initial_center[1],
                this._description.props.initial_center[0]
            ],
            this._description.props.initial_zoom
        ).on(
            'click', event => this.onMapClick({ latitude: event.latlng.lat, longitude: event.latlng.lng })
        ).on(
            'dblclick', event => this.onMapDoubleClick({ latitude: event.latlng.lat, longitude: event.latlng.lng })
        ).on(
            'zoomend', event => this.onZoom(this._leaflet_map.getZoom())
        );

        this._canvas_renderer = L.canvas({ padding: 0.5 });

        if (this._description.props.disable_doubleclick_default) {
            this._leaflet_map.doubleClickZoom.disable();
        }

        if (this._description.props.show_user_location /* && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) */) {
            const location_marker_svg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 449.998 449.998" style="enable-background:new 0 0 449.998 449.998;" xml:space="preserve"><g><polygon style="fill:#2055DA;" points="449.974,34.855 415.191,0 225.007,190.184 34.839,0 0.024,34.839 190.192,224.999  0.024,415.159 34.839,449.998 225.007,259.797 415.191,449.998 449.974,415.143 259.83,224.999" /></g></svg>';

            const location_marker_url = encodeURI("data:image/svg+xml," + location_marker_svg).replace('#','%23');

            const location_marker_icon = L.icon({
                iconUrl: location_marker_url,
                iconSize: [20, 20],
            });

            this._leaflet_map.on('locationfound', e => {
                if (this._location_radius !== undefined) {
                    this._leaflet_map.removeLayer(this._location_radius);

                    this._location_radius = undefined;
                }

                if (this._location_center !== undefined) {
                    this._leaflet_map.removeLayer(this._location_center);

                    this._location_center = undefined;
                }

                if (this._description.props.show_user_location_accuracy) {
                    this._location_radius = L.circle(e.latlng, { radius: e.accuracy/2, weight: 1, color: '#2055DA', opacity: 0.3, fillColor: '#2055DA', fillOpacity: 0.1 });

                    this._location_radius.addTo(this._leaflet_map);
                }

                this._location_center = L.marker(e.latlng, { icon: location_marker_icon });

                this._location_center.addTo(this._leaflet_map);

                this.onUserLocationUpdated(e.longitude, e.latitude, e.accuracy);
            });

            this._leaflet_map.on('locationerror', e => {
                console.warn('Location error: ' + e.message);
            });

            this._leaflet_map.locate({ watch: true });
        }

        L.tileLayer(
            this._description.props.tile_url_template,
            {
                attribution: this._description.props.attribution,
                maxZoom: this._description.props.max_zoom,
                maxNativeZoom: this._description.props.max_native_zoom,
            }
        ).addTo(this._leaflet_map);

        this._load_features();

        return;
    }

    refresh(description) {
        this._description = description;

        this._clear_features();

        this._load_features();
    }

    setZoom(zoom) {
        this._leaflet_map.setZoom(zoom);
    }

    centerOn(longitude, latitude, zoom = undefined) {
        this._leaflet_map.flyTo([latitude, longitude], zoom);
    }

    invalidateSize(pro) {
        this._leaflet_map.invalidateSize(false);
    }

    static popupFromFeature(feature) {
        return feature.properties && feature.properties.popup;
    }

    popupFromFeature(feature) {
        return this.constructor.popupFromFeature(feature);
    }

    static styleFromFeature(feature) {
        return feature.properties && feature.properties.style;
    }

    styleFromFeature(feature) {
        return this.constructor.styleFromFeature(feature);
    }

    static clusterFilter(feature) {
        return true;
    }

    clusterFilter(feature) {
        this.constructor.clusterFilter(feature);
    }

    static featureIsClickable(feature) {
        return !(feature.properties && (feature.properties.clickable === false || feature.properties.clickable === "false"));
    }

    featureIsClickable(feature) {
        return this.constructor.featureIsClickable(feature);
    }

    static featureIsDraggable(feature) {
        return !!feature.properties && (feature.properties.draggable === true || feature.properties.draggable === "true");
    }

    featureIsDraggable(feature) {
        return this.constructor.featureIsDraggable(feature);
    }

    static onUserLocationUpdated(longitude, latitude, accuracy) {
        // noop by default
    }

    onUserLocationUpdated(longitude, latitude, accuracy) {
        this.constructor.onUserLocationUpdated(longitude, latitude, accuracy);
    }

    static onFeatureClick(feature) {
        // noop by default
    }

    onFeatureClick(feature) {
        this.constructor.onFeatureClick(feature);
    }

    static onFeatureDrag(feature, longitude, latitude) {
        // noop by default
    }

    onFeatureDrag(feature, longitude, latitude) {
        this.constructor.onFeatureDrag(feature, longitude, latitude);
    }

    static onMapClick(details) {
        // noop by default
    }

    onMapClick(details) {
        this.constructor.onMapClick(details);
    }

    static onMapDoubleClick(details) {
        // noop by default
    }

    onMapDoubleClick(details) {
        this.constructor.onMapClick(details);
    }

    static onZoom(zoom) {
        // noop by default
    }

    onZoom(zoom) {
        this.constructor.onZoom(zoom);
    }
}
