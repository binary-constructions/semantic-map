import { MapDescription } from "../MapDescription";
import { createAst } from "./ast-gen.js";
import { transformAst } from "./ast-transform.js";
import * as base from "./ast-base-transforms.js";
import * as attr from "./ast-attribute-transforms.js";

export class Parser {
    constructor(root) {
        this.root = root;
    }

    parse() {
        const maps = {};

        // Create syntax tree.
        let ast = createAst(this.root, this.nodeFilter.bind(this));

        if (!ast) {
            console.warn("no semantic-map elements found");

            return undefined;
        }

        // Transform syntax tree.
        const axt = ast.map(a => transformAst(a)).filter(t => t !== undefined);

        const map_auto_id = (next => () => next++)(0);

        const context_lists = {};

        // Loop over all root elements.
        for (const x of axt) {
            for (const klass of Object.keys(x)) {
                switch (klass) {
                case "semantic-map":
                    // Add maps to list.
                    const id = x[klass].id || map_auto_id();

                    if (maps[id]) {
                        console.warn("there can be only one map with a given id (e.g. '" + id + "')");
                    } else {
                        maps[id] = x[klass];
                    }

                    break;
                case "semantic-map-context":
                    // Temporarily store context elements sorted by map id.
                    const ref = x[klass].ref;

                    if (context_lists[ref]) {
                        context_lists[ref].push(x[klass]);
                    } else {
                        context_lists[ref] = [x[klass]];
                    }

                    break;
                default:
                    console.warn("class " + klass + " doesn't work for top-level elements");
                }
            }
        }

        // Add context element data to the appropriate maps.
        for (const ref of Object.keys(context_lists)) {
            if (maps[ref]) {
                for (const context of context_lists[ref]) {
                    if (maps[ref].props.geojson_features && context.props.geojson_features) {
                        maps[ref].props.geojson_features = maps[ref].props.geojson_features.concat(context.props.geojson_features);
                    } else {
                        maps[ref].props.geojson_features = maps[ref].props.geojson_features || context.props.geojson_features;
                    }
                }
            } else {
                console.warn("map for ref '" + ref + "' couldn't be found");
            }
        }

        if (!Object.getOwnPropertyNames(maps).length) {
            console.warn("no valid map definitions found");

            return undefined;
        }

        const map_descriptions = {};

        for (const key in maps) {
            if (maps.hasOwnProperty(key)) {
                map_descriptions[key] = new MapDescription(maps[key].node, maps[key].props);
            }
        }

        return map_descriptions;
    }

    static nodeFilter(node) {
        // noop by default
        return true;
    }

    nodeFilter(node) {
        return this.constructor.nodeFilter(node);
    }
}
