import { base_transforms, base_transforms_order } from "./ast-base-transforms.js";
import { attribute_transforms, attribute_transforms_order } from "./ast-attribute-transforms.js";

// does *in-place* transformations!
function transformAttributes(attributes) {
    for (const name of attribute_transforms_order) {
        if (attributes.hasOwnProperty(name)) {
            try {
                attributes[name] = attribute_transforms[name](attributes[name], attributes);
            } catch (err) {
                console.warn("invalid value for attribute '" + name + "': '" + err + "' (value is: '" + attributes[name] + "')");

                delete attributes[name];
            }
        }
    }
}

// does *in-place* transformations!
const newRemains = (attributes, children, new_children, node) => {
    return {
        stripAttribute: (name, required = false) => {
            let attribute = undefined;

            if (attributes.hasOwnProperty(name)) {
                attribute = attributes[name];

                delete attributes[name];
            } else if (required) {
                throw "required attribute '" + name + "' is missing";
            }

            return attribute;
        },
        stripChildren: (klass, required = false, extract = false) => {
            let child_list = [];

            if (children.hasOwnProperty(klass)) {
                child_list = child_list.concat(children[klass]);

                delete children[klass];
            }

            if (new_children.hasOwnProperty(klass)) {
                child_list = child_list.concat(new_children[klass]);

                delete new_children[klass];
            }

            if (!child_list.length) {
                if (required) {
                    throw "required child '" + klass + "' is missing";
                }

                return undefined;
            } else if (extract) {
                if (child_list.length !== 1) {
                    throw "only one child of class '" + klass + "' allowed";
                }

                return child_list[0];
            } else {
                return child_list;
            }
        },
        getNode: () => node,
        getInnerHTML: () => node.innerHTML.trim().replace(/\s\s+/g, ' '),
    };
};

/**
 * Turn the provided `ASTNode` into a collection of class-specific
 * objects.
 * @param {ASTNode} ast The `ASTNode` to transform.
 * @returns {object} A dictionary of *lists* of objects, indexed by
 * their class names.
 */
export function transformAst(ast) {
    let children = {};

    // going depth-first
    if (ast.subs) {
        children = ast.subs.map(c => transformAst(c)).filter(c => c !== undefined).reduce((accu, current) => {
            for (const element of Object.keys(current)) {
                if (accu[element]) {
                    accu[element].push(current[element]);
                } else {
                    accu[element] = [ current[element] ];
                }
            }

            return accu;
        }, {});
    }

    const attributes = ast.self.attributes;

    transformAttributes(attributes);

    const classes = ast.self.classes;

    const new_children = {};

    // transform the current node
    for (const klass of base_transforms_order) {
        const index = classes.indexOf(klass);

        if (index !== -1) {
            let child_list = undefined;

            try {
                child_list = base_transforms[klass](newRemains(attributes, children, new_children, ast.self.node));
            } catch (err) {
                console.warn("failed to transform '" + klass + "': " + err);
            }

            if (child_list) {
                new_children[klass] = child_list;

                classes.splice(index, 1);
            }
        }
    }

    // hoist map and context elements
    for (const hoisted of [ "semantic-map", "semantic-map-context" ]) {
        if (children[hoisted]) {
            if (new_children[hoisted]) {
                new_children[hoisted] = children[hoisted].concat(new_children[hoisted]);
            } else {
                new_children[hoisted] = children[hoisted];
            }

            delete children[hoisted];
        }
    }

    for (const child of Object.keys(children)) {
        console.warn("stray child element: '" + child + "'");
    }

    for (const attribute of Object.keys(attributes)) {
        console.warn("stray attribute: '" + attribute + "'");
    }

    for (const klass of classes) {
        console.warn("unhandled element: '" + klass + "'");
    }

    return new_children;
}
