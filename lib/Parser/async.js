export async function load() {
    const parser = await import(/* webpackChunkName: "Parser" */ "./index");

    console.log("map parser loaded");

    return parser;
}
