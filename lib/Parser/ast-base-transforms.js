function stripProperties(remains) {
    const properties_list = remains.stripChildren("semantic-map__properties");
    const property_list = remains.stripChildren("semantic-map__property");

    const properties = {};

    if (properties_list) {
        Object.assign(properties, ...properties_list);
    }

    if (property_list) {
        Object.assign(properties, ...property_list);
    }

    return properties;
}

function stripIcons(remains) {
    const icon_list = remains.stripChildren("semantic-map__icon");

    const icons = {};

    if (icon_list) {
        Object.assign(icons, ...icon_list);
    }

    return icons;
}

function stripFeatures(remains) {
    const features_list = remains.stripChildren("semantic-map__features");
    const feature_list = remains.stripChildren("semantic-map__feature");

    let features = [];

    if (features_list) {
        features = features.concat(features_list.flat(2));
    }

    if (feature_list) {
        features = features.concat(feature_list.flat());
    }

    return features;
}

const base_transforms_ordered = [
    [ "semantic-map__attribution", remains => remains.getInnerHTML() ],
    [ "semantic-map__icon", remains => {
        const name = remains.stripAttribute("icon-name");
        const size = remains.stripAttribute("icon-size");
        const node_copy = remains.getNode().cloneNode(true);

        const children = node_copy.children;

        if (children.length !== 1 || children[0].tagName.toUpperCase() !== "SVG") {
            throw "an icon needs to have a single SVG child element";
        }

        // it seems that leaflet needs the xmlns attribute
        if (!children[0].hasAttribute('xmlns')) {
            children[0].setAttribute('xmlns', 'http://www.w3.org/2000/svg');
        }

        const icon = {};

        icon[name || ''] = {
            svg: node_copy.innerHTML,
            size: size,
        };

        return icon;
    } ],
    [ "semantic-map__properties", remains => remains.stripAttribute("properties", true) ],
    [ "semantic-map__property", remains => {
        const key = remains.stripAttribute("property-key", true);
        const value = remains.stripAttribute("property-value") || remains.getInnerHTML();
        remains.stripAttribute("property-parse"); // just so it's gone

        const property = {};

        property[key] = value;

        return property;
    } ],
    [ "semantic-map__geometries", remains => remains.stripAttribute("geometries", true) ],
    [ "semantic-map__geometry", remains => {
        const type = remains.stripAttribute("geometry-type", true);
        const geometries_list = remains.stripChildren("semantic-map__geometries");
        const geometry_list = remains.stripChildren("semantic-map__geometry");

        if (type == "GeometryCollection") {
            if (!geometries_list && !geometry_list) {
                throw "a 'GeometryCollection' needs to have child geometries";
            }

            let geometries = undefined;

            if (geometries_list && geometry_list) {
                geometries = geometries_list.flat().concat(geometry_list);
            } else if (geometries_list) {
                geometries = geometries_list.flat();
            } else {
                geometries = geometry_list;
            }

            return { type, geometries };
        } else {
            const coordinates = remains.stripAttribute("geometry-coordinates", true);

            return { type, coordinates };
        }
    } ],
    [ "semantic-map__features", remains => remains.stripAttribute("features", true) ],
    [ "semantic-map__feature", remains => ({
        type: "Feature",
        geometry: remains.stripChildren("semantic-map__geometry", true, true),
        properties: stripProperties(remains),
    }) ],
    [ "semantic-map-context", remains => ({
        ref: remains.stripAttribute("ref") || 0,
        props: {
            // ...stripProperties(remains),
            geojson_features: stripFeatures(remains),
        },
    }) ],
    [ "semantic-map", remains => ({
        node: remains.getNode(),
        id: remains.stripAttribute("id") || "",
        props: {
            // ...properties: stripProperties(remains),
            attribution: remains.stripChildren("semantic-map__attribution", false, true),
            icons: stripIcons(remains),
            disable_doubleclick_default: remains.stripAttribute("disable-doubleclick-default"),
            clustering_enabled: remains.stripAttribute("enable-clustering"),
            hide_zoom_control: remains.stripAttribute("hide-zoom-control"),
            initial_center: remains.stripAttribute("initial-center") || [0, 0],
            initial_zoom: remains.stripAttribute("initial-zoom") || 1,
            fit_features: remains.stripAttribute("fit-features"),
            min_zoom: remains.stripAttribute("min-zoom"),
            max_zoom: remains.stripAttribute("max-zoom"),
            max_native_zoom: remains.stripAttribute("max-native-zoom"),
            tile_url_template: remains.stripAttribute("tile-url-template", true),
            show_user_location: remains.stripAttribute("show-user-location"),
            show_user_location_accuracy: remains.stripAttribute("show-user-location-accuracy"),
            geojson_features: stripFeatures(remains),
        },
    }) ],
];

export const base_transforms_order = base_transforms_ordered.map(t => t[0]);

export const base_transforms = base_transforms_ordered.reduce((a, t) => {
    a[t[0]] = t[1];

    return a;
}, {});
