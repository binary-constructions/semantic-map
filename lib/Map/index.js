export class Map {
    constructor(renderer) {
        this.renderer = renderer;
    }

    mount(description) {
        this.renderer.mount(description);
    }

    refresh(description) {
        this.renderer.refresh(description);
    }
}
