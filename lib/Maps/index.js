import { Map } from "../Map";

export class Maps {
    constructor(parser, Renderer) {
        this.maps = {};
        this.parser = parser;
        this.Renderer = Renderer;
        this.is_mounted = false;
    }

    mount() {
        this.is_mounted = true;

        this.refresh();
    }

    refresh() {
        const descriptions = this.parser.parse();

        if (!descriptions) {
            console.warn("the parser came up empty");
        } else {
            for (const [ map_id, description ] of Object.entries(descriptions)) {
                if (!this.maps[map_id]) { // new map
                    this.maps[map_id] = new Map(new this.Renderer());

                    this.maps[map_id].mount(description);
                } else {        // existing map
                    this.maps[map_id].refresh(description);
                }
            }
        }
    }
}
